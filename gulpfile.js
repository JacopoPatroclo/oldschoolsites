const { watch, parallel, src, dest, series } = require("gulp");
const rollup = require("rollup");
const path = require("path");
const resolve = require("@rollup/plugin-node-resolve").default;
const babel = require("@rollup/plugin-babel").default;
const { exec } = require("child_process");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const imagemin = require("gulp-imagemin");
const connect = require("gulp-connect");
const ejs = require("gulp-ejs");

const project = process.env.PROJECT_BUILD_TARGET || "none";

console.log(`NODE_ENV: ${process.env.NODE_ENV}`);

const projectBaseDir = path.resolve(__dirname);
const projectDir = path.resolve(path.join(projectBaseDir, "sites", project));
const projectOutputDir = path.join(projectBaseDir, "builds", project);

let projectConfig = {};
try {
  projectConfig = require(path.join(projectDir, "rollup.config.js"));
} catch (error) {
  if (error.code === "MODULE_NOT_FOUND") {
    console.log("No specific configuration have been imported");
  } else {
    throw error;
  }
}

const htmls = path.join(projectDir, "**", "*.html");
const genericAssets = path.join(projectDir, "assets", "**", "*");
const genericImages = path.join(
  projectDir,
  "images",
  "**",
  "*.{svg,png,jpg,jpeg,gif}"
);

const outputImages = path.join(projectOutputDir, "images");
const outputAssets = path.join(projectOutputDir, "assets");

function rmrfBuild() {
  return exec("rm -rf builds");
}

function copyAssets() {
  return src(genericAssets).pipe(dest(outputAssets)).pipe(connect.reload());
}

function compileEjs() {
  return src(htmls)
    .pipe(ejs())
    .pipe(dest(projectOutputDir))
    .pipe(connect.reload());
}

function compileScss() {
  return src(path.join(projectDir, "*main.scss"))
    .pipe(sass().on("error", sass.logError))
    .pipe(dest(projectOutputDir))
    .pipe(connect.reload());
}

async function compileImgs() {
  return src(genericImages)
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ quality: 75, progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
        }),
      ])
    )
    .pipe(dest(outputImages))
    .pipe(connect.reload());
}

async function compileJs() {
  const entrypointJS = path.join(projectDir, "main.js");

  const bundle = await rollup.rollup({
    input: entrypointJS,
    plugins: [resolve(), babel({ babelHelpers: "bundled" })],
  });

  await bundle.write({
    dir: projectOutputDir,
    sourcemap: true,
  });
  connect.reload();
}

const build = parallel(
  rmrfBuild,
  compileJs,
  compileScss,
  compileImgs,
  copyAssets,
  compileEjs
);

exports.build = series(build, () => {
  setTimeout(() => {
    connect.serverClose();
    process.exit(0);
  }, 2000);
});

exports.watch = () => {
  watch(path.join(projectDir, "**", "*"), series(build));
  return build;
};

exports.dev = parallel(exports.watch(), () =>
  connect.server({
    host: "0.0.0.0",
    root: projectOutputDir,
    livereload: true,
    fallback: "404",
  })
);

process.on("exit", (code) => {
  console.log("Process actual exit code", code);
  process.exit(0);
});
