# Static sites are the best

A nice way to develop frameworkless old school websites

## Getting Started

This setup can support and build multiple websites. The sites are contained in the `/sites` directory.
Here each folder rapresent a different site.
With two simple command you can build or develop localy one of those sites.

First run `yarn` and make sure that you have yarn installed ghlobally on your system.
If you haven't simply run `npm i -g yarn`.

Secondly run `yarn dev:{directory_name}` to start a development server localy, the site is now visibile at `http://0.0.0.0:8080/`.

After those step you are ready to develop.

## Folder structure

The folder structure of a site is linear:

- `assets`: Inside this directroy you will put all the assets that does not need any processing during the build process, such as `.json` file.

- `images`: Here you will put all the images that need an optimiziation step.

- `main.js`: It's the entrypoint for the bundling process, done with rollup.

- `main.scss`: It's the entrypoint for the less build process.

- `index.html`: The index of your site

## Creating a new site

To create a new site just run `yarn new {site_name}`. This will create a new directory inside `sites` and a new sets of commands in `package.json`.

## EJS

Under the hood the build script support ejs. If you want to create components of reusable html you can do that by creating
some `.ejs` files that will be ignore by the build system if they are not imported by an html file. For example:

_index.html_

```
    <%- include('templates/header', { title: 'Some title' }); -%>
    <h1>
        Title
    </h1>
    <p>
        My page
    </p>
    <%- include('templates/footer'); -%>
```

_templates/header.ejs_

```
   <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>
        <% if (title) { %> <%= title %><% } else {%> No title <% } %>
        </title>
        <style>
        /* critical css go here, if you want */
        </style>
        <link rel="stylesheet" href="/main.css" />
    </head>
    <body>
```

You can find more on ejs in the template engine documentation

## Built With

- [gulp](https://gulpjs.com/) - The task runner
- [sass](https://sass-lang.com/) - For the css generation
- [rollup](https://rollupjs.org/) - For the js bundling
- [ejs](https://ejs.co/) - Ejs as template engine

## Authors

- **Jacopo Martinelli** - _Initial work_

## License

Lol use it
