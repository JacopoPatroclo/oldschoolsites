const { resolve, join } = require("path");
const { writeFileSync, mkdirSync, existsSync } = require("fs");
const packageJson = require("./package.json");

let siteName = process.argv[2];
if (!siteName || siteName.trim().length <= 0) {
  console.error("Choose a name for the project");
  process.exit(1);
}
siteName = siteName.trim();

if (existsSync(resolve(join(__dirname, "sites", siteName)))) {
  console.error(
    `A project named ${siteName} already exists, choose another name`
  );
  process.exit(1);
}

const buildCMD = [
  `build:${siteName}`,
  `NODE_ENV=production PROJECT_BUILD_TARGET=${siteName} gulp build`,
];
const devCMD = [
  `dev:${siteName}`,
  `NODE_ENV=development PROJECT_BUILD_TARGET=${siteName} gulp dev`,
];

console.log(`Creating new site named ${siteName}`);
packageJson.scripts[buildCMD[0]] = buildCMD[1];
packageJson.scripts[devCMD[0]] = devCMD[1];
writeFileSync(
  resolve(join(__dirname, "package.json")),
  JSON.stringify(packageJson, null, 2)
);
mkdirSync(resolve(join(__dirname, "sites", siteName)));
indexHtml(siteName);
mainScss(siteName);
mainjs(siteName);
assetsAndImgsDir(siteName);

function assetsAndImgsDir(projectName) {
  mkdirSync(resolve(join(__dirname, "sites", projectName, "assets")));
  mkdirSync(resolve(join(__dirname, "sites", projectName, "images")));
}

function mainjs(projectName) {
  writeFileSync(
    resolve(join(__dirname, "sites", projectName, "main.js")),
    `console.log("Hello from js, here is ${projectName}")`
  );
}

function mainScss(projectName) {
  writeFileSync(
    resolve(join(__dirname, "sites", projectName, "main.scss")),
    ""
  );
}

function indexHtml(projectName) {
  writeFileSync(
    resolve(join(__dirname, "sites", projectName, "index.html")),
    `
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${projectName}</title>
  </head>
  <body>
    <h1>Welcome to ${projectName}!!</h1>
  </body>
  </html>
  `
  );
}
